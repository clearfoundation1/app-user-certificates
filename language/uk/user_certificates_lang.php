<?php

$lang['user_certificates_app_description'] = 'Сертифікати безпеки використовуються для захисту різних програм, які ви використовуєте щодня.';
$lang['user_certificates_app_name'] = 'Сертифікати користувача';
$lang['user_certificates_certificates'] = 'Сертифікати';
$lang['user_certificates_certificates_not_available_for_this_account'] = 'Сертифікати користувача недоступні для цього облікового запису.';
$lang['user_certificates_certificates_not_available_on_this_system'] = 'На жаль, сертифікати безпеки недоступні в цій системі. Для отримання додаткової інформації зверніться до свого системного адміністратора.';
$lang['user_certificates_certificates_not_enabled_for_your_account'] = 'На жаль, сертифікати безпеки не ввімкнено для вашого облікового запису.';
$lang['user_certificates_certificates_not_yet_initialized'] = 'На жаль, систему сертифікатів безпеки ще не ініціалізовано. Для отримання додаткової інформації зверніться до свого системного адміністратора.';
$lang['user_certificates_configuration_file'] = 'Файл конфігурації';
$lang['user_certificates_create_certificates'] = 'Створити сертифікати';
$lang['user_certificates_initialize_certificate_help'] = 'Будь ласка, вкажіть пароль для захисту ваших сертифікатів безпеки. Якщо ви забули пароль, вам потрібно буде почати з новим набором сертифікатів.';
$lang['user_certificates_manage_certificates'] = 'Керування сертифікатами';
$lang['user_certificates_security_certificates'] = 'Сертифікати безпеки';
$lang['user_certificates_generate_certificates_message'] = 'Сертифікати безпеки не створено.';
$lang['user_certificates_certificates_ok_message'] = 'Сертифікати створено.';
