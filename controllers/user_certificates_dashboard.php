<?php

/**
 * User dashboard widgets controller.
 *
 * @category   apps
 * @package    user-certificates
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/user-certificates
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Exceptions
//-----------

use \Exception as Exception;
use \clearos\apps\base\Not_Found_Exception as Not_Found_Exception;

clearos_load_library('base/Not_Found_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Dashboard Widgets controller.
 *
 * @category   apps
 * @package    users
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011-2016 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/users/
 */

class User_Certificates_Dashboard extends ClearOS_Controller
{
    /**
     * Default controller
     *
     * @return string
     */

    function index()
    {
        // Bail if root
        //-------------

        $username = $this->session->userdata('username');

        if ($username === 'root')
            return;

        // Bail if not a user certificates user
        //-------------------------------------

        $this->load->factory('users/User_Factory', $username);

        $user_info = $this->user->get_info();

        if (!isset($user_info['plugins']['user_certificates']) || !$user_info['plugins']['user_certificates'])
            return;

        // Load dependencies
        //------------------

        $this->lang->load('user_certificates');
        $this->load->library('certificate_manager/User_Certificates');

        // Load the view data
        //-------------------

        try {
            $data['certificate'] = $this->user_certificates->get($this->session->userdata('username'));
        } catch (Not_Found_Exception $e) {
            $data['certificate'] = [];
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('user_certificates/user_dashboard', $data, lang('user_certificates_app_name'));

    } 
}
