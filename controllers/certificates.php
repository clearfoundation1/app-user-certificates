<?php

/**
 * User certificates controller.
 *
 * @category   apps
 * @package    user-certificates
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/user_certificates/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\mode\Mode_Engine as Mode_Engine;
use \Exception as Exception;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * User certificates controller.
 *
 * @category   apps
 * @package    user-certificates
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/user_certificates/
 */

class Certificates extends ClearOS_Controller
{
    /**
     * User certificates default controller.
     *
     * @return view
     */

    function index()
    {
        // Show account status widget if we're not in a happy state
        //---------------------------------------------------------

        $this->load->module('accounts/status');

        if ($this->status->unhappy()) {
            $this->status->widget('user_certificates');
            return;
        }

        // Bail if root
        //-------------

        $username = $this->session->userdata('username');

        if ($username === 'root') {
            $this->page->view_form('root_warning', $data, lang('user_certificates_app_name'));
            return;
        }

        // Load libraries
        //---------------

        $this->lang->load('certificate_manager');
        $this->lang->load('user_certificates');
        $this->load->library('certificate_manager/Certificate_Authority');
        $this->load->library('certificate_manager/User_Certificates');
        $this->load->factory('mode/Mode_Factory');
        $this->load->factory('users/User_Factory', $username);

        // Validation
        //-----------

        $this->form_validation->set_policy('password', 'users/User_Engine', 'validate_password', TRUE);
        $this->form_validation->set_policy('verify', 'users/User_Engine', 'validate_password', TRUE);

        $form_ok = $this->form_validation->run();

        // Extra Validation
        //------------------

        $password = ($this->input->post('password')) ? $this->input->post('password') : '';
        $verify = ($this->input->post('verify')) ? $this->input->post('verify') : '';

        if ($password != $verify) {
            $this->form_validation->set_error('verify', lang('base_password_and_verify_do_not_match'));
            $form_ok = FALSE;
        }

        // Handle form submit
        //-------------------

        if ($this->input->post('submit') && ($form_ok)) {
            try {
                $this->user_certificates->create(
                    $username,
                    $this->input->post('password'),
                    $this->input->post('verify')
                );
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the view data 
        //------------------- 

        try {
            $data['username'] = $username;
            $data['types'] = [
                'ca' => lang('certificate_manager_certificate_authority'),
                'certificate' => lang('certificate_manager_certificate'),
                'key' => lang('certificate_manager_private_key'),
                'pkcs12' => lang('certificate_manager_pkcs12')
            ];

            $cert_exists = $this->user_certificates->exists($username);
            $ca_exists = $this->certificate_authority->exists();
            $viewable = ($this->mode->get_mode() === Mode_Engine::MODE_SLAVE) ? FALSE : TRUE;

            $user_info = $this->user->get_info();
            $is_cert_user = ($user_info['plugins']['user_certificates']) ? TRUE : FALSE;
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        if (!$viewable)
            $this->page->view_form('unavailable', $data, lang('user_certificates_app_name'));
        else if (!$is_cert_user)
            $this->page->view_form('disabled', $data, lang('user_certificates_app_name'));
        else if (!$ca_exists)
            $this->page->view_form('uninitialized', $data, lang('user_certificates_app_name'));
        else if ((!$cert_exists && $username != 'root'))
            $this->page->view_form('initialize', $data, lang('user_certificates_app_name'));
        else
            $this->page->view_form('certificates', $data, lang('user_certificates_certificates'));
    }

    /**
     * Destroys certificates.
     *
     * @return view
     */

    function destroy()
    {
        $this->load->library('certificate_manager/User_Certificates');

        $username = $this->session->userdata('username');

        $this->user_certificates->delete($username);

        redirect('/user_certificates');
    }

    /**
     * Downloads certificate to requesting client.
     *
     * @param string $username  username
     * @param string $cert_type certificate type
     *
     * @return string certificate
     */

    function download($username, $cert_type)
    {
        $this->_install_download('download', $username, $cert_type);
    }

    /**
     * Installs certificate on requesting client.
     *
     * @param string $username  username
     * @param string $cert_type certificate type
     *
     * @return string certificate
     */

    function install($username, $cert_type)
    {
        $this->_install_download('install', $username, $cert_type);
    }

    /**
     * Resets all certificates.
     *
     * @return view
     */

    function reset()
    {
        $this->lang->load('certificate_manager');

        $confirm_uri = '/app/user_certificates/certificates/destroy';
        $cancel_uri = '/app/user_certificates/certificates';
        $items = array(lang('certificate_manager_security_certificates'));

        $this->page->view_confirm_delete($confirm_uri, $cancel_uri, $items);
    }

    /**
     * Common install/download method.
     *
     * @param string $type      install or download
     * @param string $username  username
     * @param string $cert_type certificate type
     *
     * @return string certificate
     */

    function _install_download($type, $username, $cert_type)
    {
        // Load dependencies
        //------------------

        $this->lang->load('certificate_manager');
        $this->load->library('certificate_manager/User_Certificates');
        $this->load->library('certificate_manager/Certificate_Authority');

        $username = $this->session->userdata('username');

        // Load view data
        //---------------

        try {
            if ($cert_type == 'ca') {
                $certificate = $this->certificate_authority->get();
                $attributes = $certificate['certificate'];
            } else {
                $certificate = $this->user_certificates->get($username);

                if ($cert_type == 'pkcs12')
                    $attributes = $certificate['pkcs12'];
                else if ($cert_type == 'certificate')
                    $attributes = $certificate['certificate'];
                else if ($cert_type == 'key')
                    $attributes = $certificate['key'];
            }
        } catch (Engine_Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load view
        //----------

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $attributes['filesize']);

        if ($type === 'download') {
            header('Content-Type: application/octet-stream');
            header("Content-Disposition: attachment; filename=" . $attributes['basename'] . ";");
        } else {
            if (! empty($attributes['pkcs12']))
                header('Content-Type: application/x-pkcs12-signature');
            else if (! empty($attributes['ca']))
                header('Content-Type: application/x-x509-ca-cert');
            else
                header('Content-Type: application/x-x509-user-cert');
        }

        echo $attributes['file_contents'];
    }
}
